#!/usr/bin/env node
import 'source-map-support/register'
import * as cdk from 'aws-cdk-lib'
import { CustomerStack } from '../lib/customer-stack'
//import { CDPipelineStack } from '../lib/cd-pipeline-stack'

const app = new cdk.App()

const STACK_NAME = 'CustomerStack'
const REPO_NAME = 'customer'

function run () {
  /* eslint-disable no-new */

  const env = { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION }

  new CustomerStack(app, STACK_NAME, {
    env,
    stackName: STACK_NAME
  }).run()

  // new CDPipelineStack(app, 'CDPipeline' + STACK_NAME, {
  //   projectName: STACK_NAME,
  //   repoName: REPO_NAME
  // })
}

run()
