import { Duration, RemovalPolicy } from 'aws-cdk-lib'
import * as awsLambda from 'aws-cdk-lib/aws-lambda'
import * as awsDynamodb from 'aws-cdk-lib/aws-dynamodb'
import * as awsIam from 'aws-cdk-lib/aws-iam'
import * as awsApigateway from 'aws-cdk-lib/aws-apigateway'
import { Construct } from 'constructs'
import {AttributeType} from "aws-cdk-lib/aws-dynamodb";

export function createApiGateway (scope: Construct, id: string, basePath: string): awsApigateway.RestApi {
  const api = new awsApigateway.RestApi(scope, id)
  const apiKey = api.addApiKey(id + ' api key')
  const usagePlan = api.addUsagePlan('usage_plan')
  usagePlan.addApiKey(apiKey)
  usagePlan.addApiStage({ stage: api.deploymentStage })
  return api
}

export function createTable (scope: Construct, tableName: string, partitionKey: awsDynamodb.Attribute): awsDynamodb.Table {
  const table = new awsDynamodb.Table(scope, tableName, {
    tableName,
    partitionKey,
    billingMode: awsDynamodb.BillingMode.PAY_PER_REQUEST,
    stream: awsDynamodb.StreamViewType.NEW_AND_OLD_IMAGES,
    removalPolicy: RemovalPolicy.RETAIN,
    pointInTimeRecovery: false
  })

  let cityIndexKey: awsDynamodb.Attribute;
  cityIndexKey = {
    name: 'city',
    type: AttributeType.STRING
  }

  let specialtyIndexKey: awsDynamodb.Attribute;
  specialtyIndexKey = {
    name: 'specialty',
    type: AttributeType.STRING
  }

  // Adding a Global Secondary Index
  table.addGlobalSecondaryIndex({
    indexName: "cityIndex",
    partitionKey: cityIndexKey,
    sortKey: specialtyIndexKey,
    projectionType: awsDynamodb.ProjectionType.ALL
  });

  table.addGlobalSecondaryIndex({
    indexName: "specialtyIndex",
    partitionKey: specialtyIndexKey,
    projectionType: awsDynamodb.ProjectionType.ALL
  });

  return table
}

export function createGoLambda (scope: Construct, id: string, path: string, env?: any, timeout?: Duration, initialPolicy?: awsIam.PolicyStatement[], retry?: any): awsLambda.Function {
  const name = pascalCase(id)
  const lambda = new awsLambda.Function(scope, name, {
    runtime: awsLambda.Runtime.PROVIDED_AL2023,
    code: awsLambda.Code.fromAsset(`build/${path}/main.zip`),
    handler: 'main',
    environment: env,
    timeout,
    initialPolicy,
    retryAttempts: retry
  })

  return lambda
}

export function createLambdaWithDynamoAccess (scope: Construct, id: string, path: string, table: awsDynamodb.Table, env?: any, initialPolicy?: awsIam.PolicyStatement[], timeout?: Duration, retry?: any): awsLambda.Function {
  const lambda = createGoLambda(scope, id, path, { ...env, TABLE_NAME: table.tableName }, timeout, initialPolicy, retry)
  const tablePolicy = new awsIam.PolicyStatement({
    actions: ['dynamodb:*'],
    resources: [table.tableArn, `${table.tableArn}/*`],
    effect: awsIam.Effect.ALLOW
  })
  lambda.addToRolePolicy(tablePolicy)

  return lambda
}

function pascalCase (text: string): string {
  text = text.replace(/[-_\s.]+(.)?/g, (_, c) => c ? c.toUpperCase() : '')
  return text.substring(0, 1).toUpperCase() + text.substring(1)
}
