import {App, aws_lambda, Duration, Stack, StackProps} from 'aws-cdk-lib'
import { Table, AttributeType } from 'aws-cdk-lib/aws-dynamodb'
import * as utils from './utils'
import {
  AuthorizationType,
  LambdaIntegration,
  RestApi,
  TokenAuthorizer,
  TokenAuthorizerProps
} from 'aws-cdk-lib/aws-apigateway'
import { DynamoEventSource } from 'aws-cdk-lib/aws-lambda-event-sources'
import { Effect, PolicyStatement } from 'aws-cdk-lib/aws-iam'
import { StartingPosition } from 'aws-cdk-lib/aws-lambda'


const TABLE_NAME = 'Customer'
const BASE_PATH = 'customer'

export class CustomerStack extends Stack {
  private readonly id: string

  constructor (scope: App, id: string, props?: StackProps) {
    super(scope, id, props)
    this.id = id
  }

  public run () {
    const table = this.createTable()
    const api = utils.createApiGateway(this, this.id + 'Api', BASE_PATH)

    this.createDoctorSearchApi(table, api)
  }

  private createTable (): Table {
    return utils.createTable(this, TABLE_NAME,
      {
        name: 'document',
        type: AttributeType.STRING
      },
    )
  }

  private createDoctorSearchApi (table: Table, api: RestApi) {
    const login = utils.createLambdaWithDynamoAccess(this, 'login', 'login', table, undefined, undefined, Duration.seconds(10))
    const create = utils.createLambdaWithDynamoAccess(this, 'create', 'create', table, undefined, undefined, Duration.seconds(10))
    const deleteCustomer = utils.createLambdaWithDynamoAccess(this, 'delete', 'delete', table, undefined, undefined, Duration.seconds(10))
    utils.createLambdaWithDynamoAccess(this, 'authorize', 'authorize', table, undefined, undefined, Duration.seconds(10))

    const resource = api.root.addResource('customers')
    resource.addMethod('POST', new LambdaIntegration(create), { apiKeyRequired: false })
    resource.addMethod('DELETE', new LambdaIntegration(deleteCustomer), { apiKeyRequired: false })
    resource.addResource('login').addMethod('POST', new LambdaIntegration(login), { apiKeyRequired: false })
  }
}
