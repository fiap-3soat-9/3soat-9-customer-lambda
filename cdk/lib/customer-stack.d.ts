import { App, Stack, StackProps } from 'aws-cdk-lib';
export declare class CustomerStack extends Stack {
    private readonly id;
    constructor(scope: App, id: string, props?: StackProps);
    run(): void;
    private createTable;
    private createDoctorSearchApi;
}
