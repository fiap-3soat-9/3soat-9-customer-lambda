package db

import (
	"3soat-9-customer-lambda/internal/model"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/mock"
	"testing"
)

type mockDynamoDBAPI struct {
	mock.Mock
	dynamodbiface.DynamoDBAPI
}

func (m *mockDynamoDBAPI) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	args := m.Called(input)
	return args.Get(0).(*dynamodb.PutItemOutput), args.Error(1)
}

func TestSave(t *testing.T) {
	mockAPI := new(mockDynamoDBAPI)
	handler := DynamoHandler{
		TableName:   "customer",
		DynamoDBAPI: mockAPI,
	}

	customer := model.Customer{
		Document: "123",
		Name:     "John Doe",
	}

	mockAPI.On("PutItem", mock.Anything).Return(&dynamodb.PutItemOutput{}, nil)

	err := handler.Save(customer)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	mockAPI.AssertExpectations(t)
}
