package main

import (
	"3soat-9-customer-lambda/internal/db"
	"3soat-9-customer-lambda/internal/jwt"
	"3soat-9-customer-lambda/internal/model"
	"3soat-9-customer-lambda/internal/response"
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
)

func HandleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	var credentials = model.Credentials{}
	if request.Body != "" {
		err := json.Unmarshal([]byte(request.Body), &credentials)
		if err != nil {
			return response.Handler(400, `{ "error": "invalid payload" }`), nil
		}
	}

	log.Println("Received event with crm: ", credentials.Username)

	dynamoHandler := db.NewDynamoHandler()

	customerReturned, err := dynamoHandler.GetByID(credentials.Username)
	if err != nil {
		return response.Handler(500, `{ "error": "failed to get customer" }`), nil
	}
	if customerReturned == nil {
		return response.Handler(401, `{ "error": "Unauthorized" }`), nil
	}

	token, err := jwt.CreateToken(credentials.Username)
	if err != nil {
		return response.Handler(500, `{ "error": "failed to create token" }`), nil
	}
	return response.Handler(200, fmt.Sprintf(`{ "token": "%s" }`, token)), nil
}

func main() {
	lambda.Start(HandleRequest)
}
