package main

import (
	request2 "3soat-9-customer-lambda/delete/request"
	"3soat-9-customer-lambda/internal/db"
	"3soat-9-customer-lambda/internal/model"
	"3soat-9-customer-lambda/internal/response"
	"context"
	"encoding/json"
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
)

func HandleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	document := request.PathParameters["document"]
	var deleteRequest = request2.CustomerDeleteRequest{}
	if request.Body != "" {
		err := json.Unmarshal([]byte(request.Body), &deleteRequest)
		if err != nil {
			return response.Handler(400, `{ "error": "invalid payload" }`), nil
		}
	}

	log.Println("Received delete event with document: ", document)

	dynamoHandler := db.NewDynamoHandler()

	customerReturned, err := dynamoHandler.GetByID(document)
	if err != nil {
		return response.Handler(500, `{ "error": "failed to get customer" }`), nil
	}
	if customerReturned == nil {
		return response.Handler(401, `{ "error": "Unauthorized" }`), nil
	}

	if validateData(deleteRequest, *customerReturned) != nil {
		return response.Handler(401, `{ "error": "Unauthorized" }`), nil
	}

	//delete
	err = dynamoHandler.DeleteById(document)
	if err != nil {
		return response.Handler(500, `{ "error": "failed to delete customer" }`), nil
	}
	return response.Handler(204, ""), nil
}

func validateData(deleteRequest request2.CustomerDeleteRequest, customer model.Customer) error {
	if deleteRequest.Name != customer.Name {
		return errors.New("name does not match")
	}
	if deleteRequest.Address != customer.Address {
		return errors.New("address does not match")
	}
	if deleteRequest.Phone != customer.Phone {
		return errors.New("phone does not match")
	}
	return nil
}

func main() {
	lambda.Start(HandleRequest)
}
