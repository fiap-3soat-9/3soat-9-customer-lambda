package main

import (
	"3soat-9-customer-lambda/internal/jwt"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
)

func generatePolicy(principalID, effect, resource string, document *string) events.APIGatewayCustomAuthorizerResponse {
	authResponse := events.APIGatewayCustomAuthorizerResponse{PrincipalID: principalID}
	if effect != "" && resource != "" {
		authResponse.PolicyDocument = events.APIGatewayCustomAuthorizerPolicy{
			Version: "2012-10-17",
			Statement: []events.IAMPolicyStatement{
				{
					Action:   []string{"execute-api:Invoke"},
					Effect:   effect,
					Resource: []string{resource},
				},
			},
		}
		if document != nil {
			authResponse.Context = map[string]interface{}{
				"document": *document,
			}
		}
	}
	return authResponse
}

func HandleRequest(ctx context.Context, request events.APIGatewayCustomAuthorizerRequest) (events.APIGatewayCustomAuthorizerResponse, error) {
	token := request.AuthorizationToken
	log.Println("Received event with token: ", token)

	document, err := jwt.VerifyToken(token)
	if err != nil {
		return generatePolicy("user", "Deny", request.MethodArn, nil), nil
	}

	return generatePolicy("user", "Allow", request.MethodArn, document), nil
}

func main() {
	lambda.Start(HandleRequest)
}
