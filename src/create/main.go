package main

import (
	"3soat-9-customer-lambda/internal/db"
	"3soat-9-customer-lambda/internal/model"
	"3soat-9-customer-lambda/internal/response"
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
)

func HandleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var customer = model.Customer{}
	if request.Body != "" {
		err := json.Unmarshal([]byte(request.Body), &customer)
		if err != nil {
			return response.Handler(400, `{ "error": "invalid payload" }`), nil
		}
	}

	log.Println("Received event with Customer: ", customer)

	dynamoHandler := db.NewDynamoHandler()
	if err := dynamoHandler.Save(customer); err != nil {
		return response.Handler(500, `{ "error": "Failed to save customer" }`), nil
	}

	return response.Handler(201, ``), nil
}

func main() {
	lambda.Start(HandleRequest)
}
