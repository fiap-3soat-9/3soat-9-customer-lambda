package response

import (
	"github.com/aws/aws-lambda-go/events"
)

func Handler(StatusCode int, Body string) events.APIGatewayProxyResponse {
	return events.APIGatewayProxyResponse{
		StatusCode: StatusCode,
		Body:       Body,
	}
}
