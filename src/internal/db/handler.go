package db

import (
	"3soat-9-customer-lambda/internal/model"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"log"
)

type Handler interface {
	Save(customer model.Customer) error
	GetByID(id string) (*model.Customer, error)
	DeleteById(id string) error
}

var handler Handler

type DynamoHandler struct {
	TableName   string
	DynamoDBAPI dynamodbiface.DynamoDBAPI
}

func NewDynamoHandler() Handler {
	if handler == nil {
		handler = &DynamoHandler{
			TableName:   "customer",
			DynamoDBAPI: GetDynamoInterface(),
		}
	}
	return handler
}

func GetDynamoInterface() dynamodbiface.DynamoDBAPI {
	dynamoSession := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	dynamoInstance := dynamodb.New(dynamoSession)

	return dynamodbiface.DynamoDBAPI(dynamoInstance)
}

func convertToDBRecord(customer model.Customer) map[string]*dynamodb.AttributeValue {
	item := map[string]*dynamodb.AttributeValue{
		"document": {S: &customer.Document},
		"name":     {S: &customer.Name},
		"address":  {S: &customer.Address},
		"phone":    {S: &customer.Phone},
		"email":    {S: &customer.Email},
	}
	return item
}

func (h *DynamoHandler) Save(customer model.Customer) error {
	input := &dynamodb.PutItemInput{
		Item:      convertToDBRecord(customer),
		TableName: aws.String(h.TableName),
	}

	savedItem, err := h.DynamoDBAPI.PutItem(input)
	if err != nil {
		log.Fatal("Failed to save customer: ", err.Error())
		return err
	}

	log.Println("Customer saved in db: ", savedItem)

	return nil
}

func (h *DynamoHandler) GetByID(id string) (*model.Customer, error) {
	input := &dynamodb.GetItemInput{
		TableName: aws.String(h.TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"document": {S: aws.String(id)},
		},
	}

	item, err := h.DynamoDBAPI.GetItem(input)
	if err != nil {
		return nil, err
	}

	if item.Item == nil {
		log.Fatal("Can't get customer by id = ", id)
		return nil, nil
	}

	var customer model.Customer
	err = dynamodbattribute.UnmarshalMap(item.Item, &customer)
	if err != nil {
		return nil, err
	}

	return &customer, nil
}

func (h *DynamoHandler) DeleteById(id string) error {
	_, err := h.DynamoDBAPI.DeleteItem(&dynamodb.DeleteItemInput{
		TableName: aws.String(h.TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"document": {S: aws.String(id)},
		},
	})
	if err != nil {
		log.Println("Customer delete in db: ", id)
	}
	return err
}
