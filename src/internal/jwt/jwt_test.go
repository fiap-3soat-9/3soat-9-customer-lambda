package jwt

import (
	"crypto/rand"
	"encoding/base64"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestCreateToken(t *testing.T) {
	key, _ := generateRandomPrivateKey(64)
	os.Setenv("PRIVATE_KEY", key)
	token, err := CreateToken("exampleUser")
	if err != nil {
		assert.Fail(t, "Failed to create token")
	}
	if token == "" {
		assert.Fail(t, "Token should not be empty")
	}
}

func TestVerifyToken(t *testing.T) {
	t.Run("Valid token", func(t *testing.T) {
		key, _ := generateRandomPrivateKey(64)
		os.Setenv("PRIVATE_KEY", key)
		token, _ := CreateToken("exampleUser")
		crm, err := VerifyToken(token)
		if err != nil {
			assert.Fail(t, "Token should be valid")
		}
		assert.Equal(t, "exampleUser", *crm)
	})
	t.Run("Invalid token", func(t *testing.T) {
		key, _ := generateRandomPrivateKey(64)
		os.Setenv("PRIVATE_KEY", key)

		// Teste com um token inválido
		_, err := VerifyToken("key")
		if err == nil {
			assert.Fail(t, "Token should be invalid")
		}
	})
}

func generateRandomPrivateKey(size int) (string, error) {
	bytes := make([]byte, size)
	_, err := rand.Read(bytes)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(bytes), nil
}
