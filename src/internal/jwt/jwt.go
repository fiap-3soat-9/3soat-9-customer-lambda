package jwt

import (
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"log"
	"os"
	"time"
)

func CreateToken(document string) (string, error) {
	secretKey := os.Getenv("PRIVATE_KEY")

	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"document": document,
		"exp":      time.Now().Add(time.Hour * 24).Unix(),
	})

	return accessToken.SignedString([]byte(secretKey))
}

func VerifyToken(tokenString string) (*string, error) {
	secretKey := os.Getenv("PRIVATE_KEY")

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(secretKey), nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		crmValue, ok := claims["document"].(string)
		if !ok {
			log.Println("error getting document value")
			return nil, errors.New("error getting document value")
		}
		if crmValue == "" {
			log.Println("empty document value")
			return nil, errors.New("empty document value")
		}
		return &crmValue, nil
	} else {
		log.Println("Invalid token")
		return nil, errors.New("invalid token")
	}
}
