# 3soat-9-customer-lambda

## How to build

`npm install`
`npm run build`

## How to deploy

First you need to authenticate into your aws account

`aws-configure`

Run the following command to bootstrap your AWS account
`cdk bootstrap`

Then you need to deploy the stack
`cdk deploy`